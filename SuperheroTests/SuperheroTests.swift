//
//  SuperheroTests.swift
//  SuperheroTests
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import XCTest
@testable import Superhero

class SuperheroTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHeroQuery() {
        let expectation = self.expectation(description: "Query for heroes")
        LocalClient.shared.loadHeroes { (heroes: [Hero], error: Error?) in
            if let error = error {
                XCTFail("Failed to retrieve heroes: \(error.localizedDescription)")
                return
            }

            XCTAssert(heroes.count > 0, "Should have more than 1")
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: 60, handler: nil)
    }

    func testUpdateHero() {
        let expectation = self.expectation(description: "Update hero rating")
        let randomRating = Hero.Rating(rawValue: Int.random(in: 0...3))!

        LocalClient.shared.loadHeroes { (heroes: [Hero], error: Error?) in
            guard let hero = heroes.first else {
                XCTFail("Failed to retrieve heroes: \(error?.localizedDescription ?? "Failed to read cache")")
                return
            }

            hero.rating = randomRating
            LocalClient.shared.updateHero(hero) {(error: Error?) in
                if let error = error {
                    XCTFail("Failed to retrieve heroes: \(error.localizedDescription)")
                    return
                }

                if let storedHero = LocalClient.shared.heroes.first(where: { $0.id == hero.id }) {
                    XCTAssert(storedHero.rating == randomRating, "Updated value")
                }

                expectation.fulfill()
            }
        }

        self.waitForExpectations(timeout: 60, handler: nil)
    }
}
