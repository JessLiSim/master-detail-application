//
//  LocalClient.swift
//  Superhero
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

private typealias JSONObject = [String: Any]

import Foundation
import RxSwift
import RxRelay

class LocalClient {
    enum LocalClientError: Error {
        case CacheError
        case NotFound
        case UnknownError
    }

    static let shared: LocalClient = LocalClient()

    private let sharedDefaults: UserDefaults = UserDefaults.shared
    private let kHeroesKey = "heroes"
    var heroes: BehaviorRelay = BehaviorRelay<[Hero]>(value: [])

    // Default data
    private lazy var defaultJSONArray: [JSONObject] = {
        [
            [
                "id": UUID().uuidString,
                "name": "Spiderman",
                "rating": 1,
                "image": "https://www.littleparties.com.au/wp-content/uploads/2017/01/Spiderman-Square-Luncheon-Plates-8Pk.jpg"
            ]
        ]
    }()

    private init() {
        if self.sharedDefaults.object(forKey: self.kHeroesKey) == nil {
            let data = self.defaultJSONArray.compactMap {
                return try? JSONSerialization.data(withJSONObject: $0, options: [])
            }

            self.sharedDefaults.set(data, forKey: self.kHeroesKey)
        }

        self.loadHeroes(completion: nil)
    }

    func loadHeroes(completion: ((_ error: Error?) -> Void)?) {
        guard
            let storedData = self.sharedDefaults.object(forKey: self.kHeroesKey) as? [Data]
        else {
            completion?(LocalClientError.UnknownError)
            return
        }

        let decoder = JSONDecoder()
        let heroes: [Hero] = storedData.compactMap {
            return try? decoder.decode(Hero.self, from: $0)
        }

        self.heroes.accept(heroes)
        completion?(nil)
    }

    func updateHero(_ hero: Hero, completion: ((_ error: Error?) -> Void)?) {
        var heroes = self.heroes.value
        guard let index: Int = heroes.firstIndex(where: { $0.id == hero.id }) else {
            completion?(LocalClientError.NotFound)
            return
        }

        heroes[index] = hero
        let encoder = JSONEncoder()
        let jsonArray = heroes.compactMap {
            return try? encoder.encode($0)
        }

        self.heroes.accept(heroes)
        self.sharedDefaults.set(jsonArray, forKey: self.kHeroesKey)
        completion?(nil)
    }
}
