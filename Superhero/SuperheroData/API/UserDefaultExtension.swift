//
//  UserDefaultExtension.swift
//  Superhero
//
//  Created by Li Sim on 16/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import Foundation

extension UserDefaults {
    static var shared: UserDefaults {
        return UserDefaults(suiteName: "group.LiSim.Superhero")!
    }
}
