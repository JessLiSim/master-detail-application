//
//  Superhero.swift
//  Superhero
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import Foundation

class Hero: Codable {
    enum Rating: Int, Codable {
        case normal = 1
        case veryGood
        case awesome

        var displayValue: String {
            switch self {
            case .normal:
                return NSLocalizedString("Normal", comment: "")
            case .veryGood:
                return NSLocalizedString("Very Good", comment: "")
            case .awesome:
                return NSLocalizedString("Awesome", comment: "")
            }
        }
    }

    var id: UUID
    var name: String
    var rating: Rating?

    var imageStr: String?
    var imageURL: URL?

    enum CodingKeys: String, CodingKey {
        case id, name, rating, imageStr = "image"
    }

    init(id: UUID, name: String, imageStr: String) {
        self.id = id
        self.name = name
        self.rating = Rating.veryGood
        self.imageStr = imageStr
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(UUID.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.rating = try container.decode(Rating.self, forKey: .rating)
        self.imageStr = try container.decodeIfPresent(String.self, forKey: .imageStr)
        self.imageURL = URL(string: imageStr ?? "")
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encodeIfPresent(self.rating, forKey: .rating)
        try container.encodeIfPresent(self.imageStr, forKey: .imageStr)
    }
}
