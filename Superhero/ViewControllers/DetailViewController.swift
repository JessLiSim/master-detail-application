//
//  DetailViewController.swift
//  Superhero
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import RxCocoa

class DetailViewController: UIViewController {
    var hero: Hero!
    private var detailView: DetailView = DetailView()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = hero.name
        self.detailView.configure(hero: self.hero)
        self.detailView.hero.asObservable().skip(1).subscribe(onNext: { hero in
            LocalClient.shared.updateHero(hero) { (error: Error?) in
                debugPrint("Error: \(String(describing: error?.localizedDescription))")

            }
        }).disposed(by: self.disposeBag)
    }

    override func loadView() {
        self.view = self.detailView
    }
}
