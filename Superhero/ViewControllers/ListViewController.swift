//
//  ViewController.swift
//  Superhero
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import RxCocoa

class ListViewController: UIViewController {
    private var listView: ListView = ListView()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Avengers", comment: "")
        self.setup()
        self.refresh()
    }

    override func loadView() {
        self.view = self.listView
    }

    func refresh() {
        LocalClient.shared.loadHeroes { (error: Error?) in
            debugPrint("Error: \(String(describing: error?.localizedDescription))")
            
        }
    }

    func setup() {
        self.listView.collectionView.register(HeroCell.self, forCellWithReuseIdentifier: HeroCell.reuseIdentifier)

        LocalClient.shared.heroes.asObservable().bind(to: self.listView.collectionView.rx.items(cellIdentifier: HeroCell.reuseIdentifier, cellType: HeroCell.self)) { (index: Int, hero: Hero, cell: HeroCell) in
            cell.configure(hero: hero)
        }.disposed(by: self.disposeBag)

        self.listView.collectionView.rx.modelSelected(Hero.self).subscribe(onNext: { hero in
            let detailVC = DetailViewController()
            detailVC.hero =  hero
            self.navigationController?.pushViewController(detailVC, animated: true)
        }).disposed(by: self.disposeBag)
        LocalClient.shared.heroes.subscribe(onNext: { _ in
            self.listView.collectionView.reloadData()
        })
    }
}

extension ListViewController {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        return CGSize(width: screenWidth, height: 130)
    }
}
