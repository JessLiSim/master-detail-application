//
//  HeroCell.swift
//  Superhero
//
//  Created by Li Sim on 14/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import UIKit
import Stevia
import SDWebImage

class HeroCell: UICollectionViewCell {
    static var reuseIdentifier: String = "HeroCell"

    private var imageView: UIImageView = UIImageView()
    private var nameLabel: UILabel = UILabel()
    private var ratingLabel: UILabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        sv(
            imageView,
            nameLabel,
            ratingLabel
        )

        let imageSize: CGFloat = 80.0
        let imageMargin: CGFloat = 16.0
        imageView.size(80).centerVertically().left(imageMargin).top(imageMargin)

        let leftMargin = imageSize + (imageMargin * 2)
        layout(
            16,
            |-leftMargin-nameLabel-16-|,
            16,
            |-leftMargin-ratingLabel-16-|,
            16
        )
    }

    func configure(hero: Hero) {
        self.nameLabel.text = hero.name
        self.ratingLabel.text = hero.rating?.displayValue
        self.imageView.sd_setImage(with: hero.imageURL, completed: nil)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
        self.nameLabel.text = nil
        self.ratingLabel.text = nil
    }
}
