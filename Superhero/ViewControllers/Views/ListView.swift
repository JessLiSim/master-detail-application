//
//  ListView.swift
//  Superhero
//
//  Created by Li Sim on 16/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import UIKit

class ListView: UIView {
    var collectionView: UICollectionView!
    let refreshControl = UIRefreshControl()

    convenience init() {
        self.init(frame:CGRect.zero)
        self.backgroundColor = .white

        // Layout
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 112)

        // CollectionView init
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        sv(collectionView)
        collectionView.fillContainer()
        collectionView.addSubview(self.refreshControl)

        self.collectionView = collectionView
    }
}
