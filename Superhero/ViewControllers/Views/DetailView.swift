//
//  DetailView.swift
//  Superhero
//
//  Created by Li Sim on 16/05/2019.
//  Copyright © 2019 Li Sim. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import Stevia
import SDWebImage
import Cosmos

class DetailView: UIView {
    var hero: BehaviorRelay<Hero>!

    private var imageView: UIImageView = UIImageView()
    private var ratingView: CosmosView = CosmosView()

    convenience init() {
        self.init(frame:CGRect.zero)
        self.backgroundColor = .white

        // Star rating settings
        self.ratingView.settings.starSize = 44
        self.ratingView.settings.totalStars = 3
        self.ratingView.didFinishTouchingCosmos = {(rating: Double) in
            if let rating = Hero.Rating(rawValue: Int(rating)) {
                let hero = self.hero.value
                hero.rating = rating
                self.hero.accept(hero)
            }
        }

        // Layout
        sv(imageView, ratingView)

        let screenWidth = UIScreen.main.bounds.width
        self.imageView.size(screenWidth)

        layout(
            imageView.Top == safeAreaLayoutGuide.Top,
            imageView,
            16,
            ratingView.height(44).centerHorizontally()
        )
    }

    func configure(hero: Hero) {
        self.hero = BehaviorRelay(value: hero)
        self.imageView.sd_setImage(with: hero.imageURL, completed: nil)
        if let rating = hero.rating {
            self.ratingView.rating = Double(rating.rawValue)
        }
    }
}
